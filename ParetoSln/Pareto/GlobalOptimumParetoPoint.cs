﻿using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace Pareto
{
    internal class GlobalOptimumParetoPoint : ParetoPoint
    {
        public GlobalOptimumParetoPoint(object tag, Point2D point, OxyColor color, MarkerType markerType)
            : base(tag, point, color, markerType, "Global optimum")
        {
        }
    }
}