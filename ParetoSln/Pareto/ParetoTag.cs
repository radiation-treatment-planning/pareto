﻿using System.Collections.Generic;

namespace Pareto
{
    public class ParetoTag
    {
        private readonly List<(string key, string value)> _keyValuePairs;

        protected ParetoTag(List<(string key, string value)> keyValuePairs)
        {
            _keyValuePairs = keyValuePairs;
        }

        public override string ToString()
        {
            var str = "";
            foreach (var keyValuePair in _keyValuePairs) str += $"{keyValuePair.key}:\t{keyValuePair.value}\n";

            // Cut off last \n if string is not empty.
            if (str.Length > 0) str = str.Substring(0, str.Length - 1);
            return str;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != GetType()) return false;

            var other = (ParetoTag)obj;
            return ToString() == other.ToString();
        }

        public override int GetHashCode()
        {
            int hash = 0;
            foreach (var keyValuePair in _keyValuePairs)
                hash ^= keyValuePair.key.GetHashCode() ^ keyValuePair.value.GetHashCode();
            return hash;
        }
        public class Factory
        {
            public static ParetoTag CreateParetoTag(List<(string key, string value)> keyValuePairs)
            {
                return new ParetoTag(keyValuePairs);
            }
        }
    }
}
