﻿using System;
using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace Pareto
{
    public partial class ParetoPoint
    {
        public class Factory
        {
            public static ParetoPoint CreateParetoPoint(object tag, Point2D point, OxyColor color,
                MarkerType markerType, string description = "Pareto point")
            {
                if (tag == null) throw new ArgumentNullException(nameof(tag));
                if (description == null) throw new ArgumentNullException(nameof(description));
                return new ParetoPoint(tag, point, color, markerType, description);
            }

            public static ParetoPoint CreateIllusionParetoPoint(object tag, Point2D point, OxyColor color,
                MarkerType markerType)

            {
                if (tag == null) throw new ArgumentNullException(nameof(tag));
                return new IllusionParetoPoint(tag, point, color, markerType);
            }

            public static ParetoPoint CreateDominatingParetoPoint(object tag, Point2D point, OxyColor color,
                MarkerType markerType)
            {
                if (tag == null) throw new ArgumentNullException(nameof(tag));
                return new DominatingParetoPoint(tag, point, color, markerType);
            }

            public static ParetoPoint CreateGlobalOptimumParetoPoint(object tag, Point2D point, OxyColor color,
                MarkerType markerType)
            {
                if (tag == null) throw new ArgumentNullException(nameof(tag));
                return new GlobalOptimumParetoPoint(tag, point, color, markerType);
            }
            public static ParetoPoint CreateUniqueParetoPoint(object tag, Point2D point, OxyColor color,
                MarkerType markerType, string description)
            {
                if (tag == null) throw new ArgumentNullException(nameof(tag));
                if (description == null) throw new ArgumentNullException(nameof(description));
                return new UniqueParetoPoint(tag, point, color, markerType, description);
            }

        }
    }
}
