﻿using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace Pareto
{
    public partial class ParetoLine
    {
        public class Factory
        {
            public static ParetoLine CreateParetoLine(object tag, Point2D[] points, OxyColor color, LineStyle lineStyle)
            {
                return new ParetoLine(tag, points, color, lineStyle, "Pareto line");
            }
            public static ParetoLine CreateParetoFrontLine(object tag, Point2D[] points, OxyColor color, LineStyle lineStyle)
            {
                return new ParetoFrontLine(tag, points, color, lineStyle);
            }
            public static ParetoLine CreateDiagonalLine(object tag, Point2D[] points, OxyColor color, LineStyle lineStyle)
            {
                return new DiagonalLine(tag, points, color, lineStyle);
            }
        }
    }
}
