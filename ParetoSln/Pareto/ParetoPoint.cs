﻿using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace Pareto
{
    public partial class ParetoPoint
    {
        public object Tag { get; }
        public Point2D Point { get; }
        public OxyColor Color { get; }
        public MarkerType MarkerType { get; }
        public string Description { get; }

        protected ParetoPoint(object tag, Point2D point, OxyColor color, MarkerType markerType, string description)
        {
            Tag = tag;
            Point = point;
            Color = color;
            MarkerType = markerType;
            Description = description;
        }
    }
}