﻿using System.Collections.Generic;
using System.Linq;

namespace Pareto
{
    public class Pareto
    {
        private readonly ParetoPoint[] _points;
        private readonly ParetoLine[] _lines;
        public IEnumerable<ParetoPoint> Points => _points.AsEnumerable();
        public IEnumerable<ParetoLine> Lines => _lines.AsEnumerable();

        public Pareto(ParetoPoint[] points, ParetoLine[] lines)
        {
            _points = points;
            _lines = lines;
        }
    }
}
