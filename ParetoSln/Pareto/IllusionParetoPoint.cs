﻿using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace Pareto
{
    internal class IllusionParetoPoint : ParetoPoint
    {
        public IllusionParetoPoint(object tag, Point2D point, OxyColor color, MarkerType markerType)
            : base(tag, point, color, markerType, "Illusion point")
        {
        }
    }
}