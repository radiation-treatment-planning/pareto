﻿using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace Pareto
{
    internal class UniqueParetoPoint : ParetoPoint
    {
        public UniqueParetoPoint(object tag, Point2D point, OxyColor color, MarkerType markerType, string description)
            : base(tag, point, color, markerType, description)
        {
        }
    }
}
