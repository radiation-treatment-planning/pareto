﻿using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace Pareto
{
    internal class ParetoFrontLine : ParetoLine
    {
        public ParetoFrontLine(object tag, Point2D[] points, OxyColor color, LineStyle lineStyle) 
            : base(tag, points, color, lineStyle, "Pareto front line")
        {
        }
    }
}