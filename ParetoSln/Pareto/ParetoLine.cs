﻿using System;
using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace Pareto
{
    public partial class ParetoLine
    {
        public object Tag { get; }
        public Point2D[] Points { get; }
        public OxyColor Color { get; }
        public LineStyle LineStyle { get; }
        public string Description { get; }

        protected ParetoLine(object tag, Point2D[] points, OxyColor color, LineStyle lineStyle, string description)
        {
            if (points.Length < 2)
                throw new ArgumentException(
                    $"Pareto lines can only consist of two or more points, but {points.Length} were given.");
            Tag = tag;
            Points = points;
            Color = color;
            LineStyle = lineStyle;
            Description = description;
        }
    }
}