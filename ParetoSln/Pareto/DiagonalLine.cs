﻿using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace Pareto
{
    internal class DiagonalLine : ParetoLine
    {
        public DiagonalLine(object tag, Point2D[] points, OxyColor color, LineStyle lineStyle)
            : base(tag, points, color, lineStyle, "Diagonal line")
        {
        }
    }
}