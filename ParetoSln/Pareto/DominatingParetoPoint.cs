﻿using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace Pareto
{
    internal class DominatingParetoPoint : ParetoPoint
    {
        public DominatingParetoPoint(object tag, Point2D point, OxyColor color, MarkerType markerType)
            : base(tag, point, color, markerType, "Dominating point")
        {
        }
    }
}