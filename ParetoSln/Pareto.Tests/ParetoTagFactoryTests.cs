﻿using System.Collections.Generic;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;

namespace Pareto.Tests
{
    [TestFixture]
    public class ParetoTagFactoryTests
    {
        [Test]
        public void CreateParetoTag_Test()
        {
            var tag = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key1", "value1"), ("key2", "value2")});

            var stringOfTag = "key1:\tvalue1\nkey2:\tvalue2";

            Assert.AreEqual(1975005998, tag.GetHashCode());
            Assert.AreEqual(stringOfTag, tag.ToString());
        }

        [Test]
        public void Equals_ReturnTrueIfObjectsAreEqual_Test()
        {
            var tag = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key1", "value1"), ("key2", "value2")});
            var tag2 = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key1", "value1"), ("key2", "value2")});

            Assert.AreEqual(tag, tag2);
        }

        [Test]
        public void Equals_ReturnFalseIfObjectsAreNotEqual_Test()
        {
            var tag = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key1", "value1"), ("key2", "value2")});
            var tag2 = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key1", "value1")});
            ParetoTag tag3 = null;
            var point = new Point2D(0, 1);

            Assert.AreNotEqual(tag, tag2);
            Assert.AreNotEqual(tag, tag3);
            Assert.AreNotEqual(tag, point);
        }

        [Test]
        public void CreateParetoTag_WithEmptyInputList_()
        {
            var tag = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>() { });
            var toString = tag.ToString();
            Assert.AreEqual("", toString);
        }
    }
}
