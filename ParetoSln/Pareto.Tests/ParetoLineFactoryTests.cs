﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace Pareto.Tests
{
    [TestFixture]
    public class ParetoLineFactoryTests
    {
        [Test]
        public void CreateParetoLine_Test()
        {
            var points = new Point2D[]
            {
                new Point2D(0.1, 0.9),
                new Point2D(0.3, 0.6),
                new Point2D(0.6, 0.3),
                new Point2D(0.1, 0.9),
            };

            var tag = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key1", "value1"), ("key2", "value2")});
            var paretoLine = ParetoLine.Factory.CreateParetoLine(
                tag, points, OxyColors.AliceBlue, LineStyle.Automatic);

            Assert.AreEqual(LineStyle.Automatic, paretoLine.LineStyle);
            Assert.AreEqual(OxyColors.AliceBlue, paretoLine.Color);
            Assert.AreEqual("Pareto line", paretoLine.Description);
            Assert.AreEqual(points.Length, paretoLine.Points.Length);
            foreach (var point in points) Assert.Contains(point, paretoLine.Points);
            Assert.AreEqual(tag, paretoLine.Tag);
        }

        [Test]
        public void CreateParetoFrontLine_Test()
        {

            var points = new Point2D[]
            {
                new Point2D(0.1, 0.9),
                new Point2D(0.3, 0.6),
                new Point2D(0.6, 0.3),
                new Point2D(0.1, 0.9),
            };

            var tag = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key1", "value1"), ("key2", "value2")});
            var paretoFrontLine = ParetoLine.Factory.CreateParetoFrontLine(
                tag, points, OxyColors.AliceBlue, LineStyle.Automatic);

            Assert.AreEqual(LineStyle.Automatic, paretoFrontLine.LineStyle);
            Assert.AreEqual(OxyColors.AliceBlue, paretoFrontLine.Color);
            Assert.AreEqual("Pareto front line", paretoFrontLine.Description);
            Assert.AreEqual(points.Length, paretoFrontLine.Points.Length);
            foreach (var point in points) Assert.Contains(point, paretoFrontLine.Points);
            Assert.AreEqual(tag, paretoFrontLine.Tag);
        }

        [Test]
        public void CreateDiagonalLine_Test()
        {

            var points = new Point2D[]
            {
                new Point2D(0.1, 0.9),
                new Point2D(0.3, 0.6),
                new Point2D(0.6, 0.3),
                new Point2D(0.1, 0.9),
            };

            var tag = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key1", "value1"), ("key2", "value2")});
            var diagonalLine = ParetoLine.Factory.CreateDiagonalLine(
                tag, points, OxyColors.AliceBlue, LineStyle.Automatic);

            Assert.AreEqual(LineStyle.Automatic, diagonalLine.LineStyle);
            Assert.AreEqual(OxyColors.AliceBlue, diagonalLine.Color);
            Assert.AreEqual("Diagonal line", diagonalLine.Description);
            Assert.AreEqual(points.Length, diagonalLine.Points.Length);
            foreach (var point in points) Assert.Contains(point, diagonalLine.Points);
            Assert.AreEqual(tag, diagonalLine.Tag);
        }

        [Test]
        public void CreateParetoLine_ThrowArgumentExceptionIfLessThanTwoPointsWereGivenAsArgument_Test()
        {
            var points = new Point2D[] { };
            Assert.Throws<ArgumentException>(() =>
                ParetoLine.Factory.CreateParetoLine("", points, OxyColors.AliceBlue, LineStyle.Automatic));
            Assert.Throws<ArgumentException>(() =>
                ParetoLine.Factory.CreateDiagonalLine("", points, OxyColors.AliceBlue, LineStyle.Automatic));
            Assert.Throws<ArgumentException>(() =>
                ParetoLine.Factory.CreateParetoFrontLine("", points, OxyColors.AliceBlue, LineStyle.Automatic));
        }
    }
}
