﻿using System.Collections.Generic;
using System.Linq;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using OxyPlot;

namespace Pareto.Tests
{
    [TestFixture]
    public class ParetoTests
    {
        [Test]
        public void Constructor_Test()
        {
            var paretoLines = CreateParetoLines();
            var paretoPoints = CreateParetoPoints();
            var pareto = new Pareto(paretoPoints, paretoLines);

            Assert.AreEqual(paretoPoints.Length, pareto.Points.Count());
            Assert.AreEqual(paretoLines.Length, pareto.Lines.Count());

            foreach (var point in paretoPoints) Assert.Contains(point, pareto.Points.ToList());
            foreach (var paretoLine in paretoLines) Assert.Contains(paretoLine, pareto.Lines.ToList());
        }

        private ParetoPoint[] CreateParetoPoints()
        {
            var tag = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key1", "value1"), ("key2", "value2")});
            var paretoPoint =
                ParetoPoint.Factory.CreateParetoPoint(tag, Point2D.Origin, OxyColors.Aqua, MarkerType.Cross);
            var dominatingParetoPoint =
                ParetoPoint.Factory.CreateDominatingParetoPoint(tag, Point2D.Origin, OxyColors.Aqua, MarkerType.Cross);
            var illusionParetoPoint =
                ParetoPoint.Factory.CreateIllusionParetoPoint(tag, Point2D.Origin, OxyColors.Aqua, MarkerType.Cross);
            return new[] { paretoPoint, dominatingParetoPoint, illusionParetoPoint };
        }

        private ParetoLine[] CreateParetoLines()
        {
            var points = new Point2D[]
            {
                new Point2D(0.1, 0.9),
                new Point2D(0.3, 0.6),
                new Point2D(0.6, 0.3),
                new Point2D(0.1, 0.9),
            };

            var tag1 = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key1", "value1"), ("key2", "value2")});
            var tag2 = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key3", "value3")});
            var paretoLine = ParetoLine.Factory.CreateParetoLine(
                tag1, points, OxyColors.AliceBlue, LineStyle.Automatic);
            var diagonalLine = ParetoLine.Factory.CreateDiagonalLine(
                tag2, points, OxyColors.AliceBlue, LineStyle.Automatic);
            var paretoFrontLine = ParetoLine.Factory.CreateParetoFrontLine(
                tag2, points, OxyColors.AliceBlue, LineStyle.Automatic);
            return new[] { paretoLine, diagonalLine, paretoFrontLine };
        }
    }
}
