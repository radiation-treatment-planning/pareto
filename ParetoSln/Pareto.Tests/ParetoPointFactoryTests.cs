﻿using System;
using System.Collections.Generic;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using OxyPlot;

namespace Pareto.Tests
{
    [TestFixture]
    public class ParetoPointFactoryTests
    {
        [Test]
        public void CreateParetoPoint_Test()
        {
            var tag = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key1", "value1"), ("key2", "value2")});
            var paretoPoint =
                ParetoPoint.Factory.CreateParetoPoint(tag, Point2D.Origin, OxyColors.Aqua, MarkerType.Cross);

            Assert.AreEqual(OxyColors.Aqua, paretoPoint.Color);
            Assert.AreEqual("Pareto point", paretoPoint.Description);
            Assert.AreEqual(MarkerType.Cross, paretoPoint.MarkerType);
            Assert.AreEqual(Point2D.Origin, paretoPoint.Point);
            Assert.AreEqual(tag, paretoPoint.Tag);
        }

        [Test]
        public void CreateParetoPoint_WithCustomDescription_Test()
        {
            var tag = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key1", "value1"), ("key2", "value2")});
            var paretoPoint =
                ParetoPoint.Factory.CreateParetoPoint(tag, Point2D.Origin, OxyColors.Aqua, MarkerType.Cross, "Custom description");

            Assert.AreEqual(OxyColors.Aqua, paretoPoint.Color);
            Assert.AreEqual("Custom description", paretoPoint.Description);
            Assert.AreEqual(MarkerType.Cross, paretoPoint.MarkerType);
            Assert.AreEqual(Point2D.Origin, paretoPoint.Point);
            Assert.AreEqual(tag, paretoPoint.Tag);
        }

        [Test]
        public void CreateParetoPoint_ThrowArgumentNullExceptionIfDescriptionIsNull_Test()
        {
            var tag = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key1", "value1"), ("key2", "value2")});

            Assert.Throws<ArgumentNullException>(() =>
                ParetoPoint.Factory.CreateParetoPoint(tag, Point2D.Origin, OxyColors.Aqua, MarkerType.Cross,
                    null));
        }

        [Test]
        public void CreateParetoPoint_ThrowArgumentNullExceptionIfTagIsNull_Test()
        {
            Assert.Throws<ArgumentNullException>(() =>
                ParetoPoint.Factory.CreateParetoPoint(null, Point2D.Origin, OxyColors.Aqua, MarkerType.Cross,
                    ""));
        }

        [Test]
        public void CreateDominatingParetoPoint_Test()
        {
            var tag = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key1", "value1"), ("key2", "value2")});
            var dominatingParetoPoint =
                ParetoPoint.Factory.CreateDominatingParetoPoint(tag, Point2D.Origin, OxyColors.Aqua, MarkerType.Cross);

            Assert.AreEqual(OxyColors.Aqua, dominatingParetoPoint.Color);
            Assert.AreEqual("Dominating point", dominatingParetoPoint.Description);
            Assert.AreEqual(MarkerType.Cross, dominatingParetoPoint.MarkerType);
            Assert.AreEqual(Point2D.Origin, dominatingParetoPoint.Point);
            Assert.AreEqual(tag, dominatingParetoPoint.Tag);
        }

        [Test]
        public void CreateDominatingParetoPoint_ThrowArgumentNullExceptionForNullTag_Test()
        {
            Assert.Throws<ArgumentNullException>(() =>
                ParetoPoint.Factory.CreateDominatingParetoPoint(null, Point2D.Origin, OxyColors.Aqua,
                    MarkerType.Cross));
        }

        [Test]
        public void CreateIllusionPoint_Test()
        {
            var tag = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key1", "value1"), ("key2", "value2")});
            var illusionParetoPoint =
                ParetoPoint.Factory.CreateIllusionParetoPoint(tag, Point2D.Origin, OxyColors.Aqua, MarkerType.Cross);

            Assert.AreEqual(OxyColors.Aqua, illusionParetoPoint.Color);
            Assert.AreEqual("Illusion point", illusionParetoPoint.Description);
            Assert.AreEqual(MarkerType.Cross, illusionParetoPoint.MarkerType);
            Assert.AreEqual(Point2D.Origin, illusionParetoPoint.Point);
            Assert.AreEqual(tag, illusionParetoPoint.Tag);
        }

        [Test]
        public void CreateIllusionPoint_ThrowArgumentNullExceptionForNullTag_Test()
        {
            Assert.Throws<ArgumentNullException>(() =>
                ParetoPoint.Factory.CreateIllusionParetoPoint(null, Point2D.Origin, OxyColors.Aqua,
                    MarkerType.Cross));
        }

        [Test]
        public void CreateGlobalOptimumParetoPoint_Test()
        {
            var tag = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key1", "value1"), ("key2", "value2")});
            var globalOptimumParetoPoint =
                ParetoPoint.Factory.CreateGlobalOptimumParetoPoint(tag, Point2D.Origin, OxyColors.Aqua, MarkerType.Cross);

            Assert.AreEqual(OxyColors.Aqua, globalOptimumParetoPoint.Color);
            Assert.AreEqual("Global optimum", globalOptimumParetoPoint.Description);
            Assert.AreEqual(MarkerType.Cross, globalOptimumParetoPoint.MarkerType);
            Assert.AreEqual(Point2D.Origin, globalOptimumParetoPoint.Point);
            Assert.AreEqual(tag, globalOptimumParetoPoint.Tag);
        }

        [Test]
        public void CreateGlobalOptimumParetoPoint_ThrowArgumentNullExceptionForNullTag_Test()
        {
            Assert.Throws<ArgumentNullException>(() =>
                ParetoPoint.Factory.CreateGlobalOptimumParetoPoint(null, Point2D.Origin, OxyColors.Aqua,
                    MarkerType.Cross));
        }

        [Test]
        public void CreateUniqueParetoPoint_Test()
        {
            var tag = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {("key1", "value1"), ("key2", "value2")});
            var globalOptimumParetoPoint =
                ParetoPoint.Factory.CreateUniqueParetoPoint(tag, Point2D.Origin, OxyColors.Aqua, MarkerType.Cross, "Clinical plan");

            Assert.AreEqual(OxyColors.Aqua, globalOptimumParetoPoint.Color);
            Assert.AreEqual("Clinical plan", globalOptimumParetoPoint.Description);
            Assert.AreEqual(MarkerType.Cross, globalOptimumParetoPoint.MarkerType);
            Assert.AreEqual(Point2D.Origin, globalOptimumParetoPoint.Point);
            Assert.AreEqual(tag, globalOptimumParetoPoint.Tag);
        }

        [Test]
        public void CreateUniqueParetoPoint_ThrowArgumentNullException_ForNullStringInput_Test()
        {
            Assert.Throws<ArgumentNullException>(() =>
                ParetoPoint.Factory.CreateUniqueParetoPoint("", Point2D.Origin, OxyColors.Aqua, MarkerType.Cross,
                    null));
        }

        [Test]
        public void CreateUniqueParetoPoint_ThrowArgumentNullExceptionForNullTag_Test()
        {
            Assert.Throws<ArgumentNullException>(() =>
                ParetoPoint.Factory.CreateUniqueParetoPoint(null, Point2D.Origin, OxyColors.Aqua, MarkerType.Cross,
                    ""));
        }
    }
}
